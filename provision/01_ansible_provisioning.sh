#!/bin/bash

#set config
export ANSIBLE_CONFIG=/home/vagrant/ansible/config/ansible.cfg

#install required roles
ansible-galaxy install -r /vagrant/provision/01_ansible_requirements.yml --roles-path /home/vagrant/ansible/roles
chown -R vagrant:vagrant /home/vagrant/ansible

#start ansible provisioning
ansible-playbook /vagrant/provision/02_site.yml -vv
