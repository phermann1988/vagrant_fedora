#!/bin/bash

IP=$1
HOSTNAME=$2

pip install ansible
yum -y install sshpass
mkdir -p /home/vagrant/ansible/{config,roles,inventory}

cat >/home/vagrant/ansible/inventory/hosts<<EOF
${HOSTNAME} ansible_ssh_host=${IP} ansible_user=vagrant ansible_ssh_pass=vagrant ansible_host=${IP}
EOF

cat >/home/vagrant/ansible/config/ansible.cfg<<EOF
[defaults]
inventory = /home/vagrant/ansible/inventory/hosts
roles_path = /home/vagrant/ansible/roles
host_key_checking = False
[privilege_escalation]
become=True
become_method=sudo
become_user=root
become_ask_pass=False
EOF

echo "export ANSIBLE_CONFIG=/home/vagrant/ansible/config/ansible.cfg" | tee -a /home/vagrant/.bashrc | tee -a /root/.bashrc

chown -R vagrant:vagrant /home/vagrant/ansible
